using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine.Events;

public class ChefAgent : Agent
{

    [SerializeField]
    UnityEvent _episodeEnd = default;
    public event UnityAction episodeEnd
    {
        add { _episodeEnd.AddListener(value); }
        remove { _episodeEnd.RemoveListener(value); }
    }
    void OnEpisodeEnd()
    {
        _episodeEnd.Invoke();
    }

    Rigidbody2D rb;
    ObservationCollector _observationCollector;
    ChefStateMachine _stateMachine;
    Interactor _interactor;
    IMover _mover;
    IInputManager _inputManager;
    IVisuals _visuals;
    IRewarder _agentRewarder;
    bool wasInteractKeyPressed = false;
    ChefAgentPropertyChanger _chefAgentPropertyChanger = default;

    public override void OnEpisodeBegin()
    {
        //Temporary solution to Academy.Instance.OnEnvironmentReset
        // not calling when max steps is reached.
        OnEpisodeEnd();
        //NO LONGER NEEDED SINCE SPAWN POSITIONS ARE RANDOMISED
        //gameObject.transform.localPosition = _spawnPoint.localPosition; 
        rb.velocity = new Vector3(0, 0, 0);
        if (_chefAgentPropertyChanger != null)
        {
            _chefAgentPropertyChanger.RandomiseModel();
        }
        //_stateMachine.SetHeldItem(Item.None);
    }

    public override void Heuristic(float[] actionsOut)
    {
        _inputManager.SetActions(actionsOut);
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        _observationCollector.AddObservations(sensor);
    }

    public override void OnActionReceived(float[] act)
    {
        if (_inputManager.GetAcceptInputs())
        {
            _mover.Move(act);
            _visuals.UpdateVisuals(act);

            if (act[1] == (int)Action.Interact)
            {
                if (!wasInteractKeyPressed)
                {
                    _interactor.Interact(_stateMachine);
                    wasInteractKeyPressed = true;
                }
            }
            else
            {
                wasInteractKeyPressed = false;
            }
        }
    }

    // Start is called before the first frame update
    public override void Initialize()
    {
        _observationCollector = GetComponent<ObservationCollector>();
        _interactor = GetComponent<Interactor>();
        _stateMachine = GetComponent <ChefStateMachine>();
        _mover = GetComponent<IMover>();
        _inputManager = GetComponent<IInputManager>();
        _agentRewarder = GetComponent<IRewarder>();
        _visuals = GetComponentInChildren<IVisuals>();
        rb = GetComponent<Rigidbody2D>();
        _chefAgentPropertyChanger = GetComponent<ChefAgentPropertyChanger>();
    }

    public void RewardAgent(float reward)
    {
        _agentRewarder.AddReward(reward);
    }
}
public enum Movement
{
    NoAction,
    Up,
    Down,
    Left,
    Right
}

public enum Action
{
    NoAction,
    Interact
}