﻿using Unity.Barracuda;
using Unity.MLAgents.Policies;
using UnityEngine;

internal class ChefAgentPropertyChanger : MonoBehaviour
{
    IInputManager _inputManager = default;
    BehaviorParameters _behaviourParameters = default;

    [SerializeField]
    NNModel[] _nNModels = default;
    public void RandomiseModel()
    {
        NNModel chosenModel = _nNModels[Random.Range(0, _nNModels.Length)];
        if (chosenModel != null) {
            _inputManager.SetAcceptInputs(true);
            _behaviourParameters.Model = chosenModel;
        }
        else
        {
            //Substitute for inanimate model;
            _inputManager.SetAcceptInputs(false);
        }
    }

    private void Awake()
    {
        _behaviourParameters = GetComponent<BehaviorParameters>();
        _inputManager = GetComponent<IInputManager>();
    }
}