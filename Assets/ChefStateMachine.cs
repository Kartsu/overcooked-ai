using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChefStateMachine : MonoBehaviour
{

    [SerializeField]
    UnityEvent m_StateChanged = default;
    public event UnityAction StateChanged
    {
        add { m_StateChanged.AddListener(value); }
        remove { m_StateChanged.RemoveListener(value); }
    }
    void OnStateChanged()
    {
        m_StateChanged.Invoke();
    }

    Item _heldItem = Item.None;

    Movement _previousMoveDirection = Movement.Up;

    public void SetHeldItem(Item item)
    {
        _heldItem = item;
        OnStateChanged();
    }

    public Item GetHeldItem()
    {
        return _heldItem;
    }

    public void SetPreviousMoveDirection(Movement movement)
    {
        _previousMoveDirection = movement;
    }

    public Movement GetPreviousMoveDirection()
    {
        return _previousMoveDirection;
    }
}

public enum Item
{
    None,
    Tomato,
    Plate,
    TomatoSoup,
}