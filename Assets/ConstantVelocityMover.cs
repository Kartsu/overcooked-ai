﻿using UnityEngine;
using static ChefAgent;

internal class ConstantVelocityMover : MonoBehaviour, IMover
{
    Rigidbody2D rb;
    [SerializeField]
    float _moveSpeed = 1;

    public void Move(float[] act)
    {
        int _movement = Mathf.FloorToInt(act[0]);
        // Look up the index in the movement action list:
        if (_movement == (int) Movement.NoAction)
        {
            Stop();
        }
        if (_movement == (int) Movement.Left)
        {
            MoveLeft();
        }
        if (_movement == (int) Movement.Right)
        {
            MoveRight();
        }
        if (_movement == (int) Movement.Down)
        {
            MoveDown();
        }
        if (_movement == (int) Movement.Up)
        {
            MoveUp();
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void MoveDown()
    {
        rb.velocity = new Vector2(0, -_moveSpeed);
    }

    void MoveLeft()
    {
        rb.velocity = new Vector2(-_moveSpeed, 0);
    }

    void MoveRight()
    {
        rb.velocity = new Vector2(_moveSpeed, 0);
    }

    void MoveUp()
    {
        rb.velocity = new Vector2(0, _moveSpeed);
    }

    void Stop()
    {
        rb.velocity = new Vector2(0, 0);
    }
}