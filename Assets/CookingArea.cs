using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.MLAgents;
using UnityEngine;

public class CookingArea : MonoBehaviour
{
    [SerializeField]
    Transform[] _spawnPoints = default;
    ChefAgent[] _chefAgents = default;
    public void Awake()
    {
        Academy.Instance.OnEnvironmentReset += EnvironmentReset;
        _chefAgents = GetComponentsInChildren<ChefAgent>();
        Debug.Assert(_spawnPoints.Length >= _chefAgents.Length);
    }

    public void EnvironmentReset()
    {
        // Reset the scene here
        Randomizer.Randomize(_spawnPoints);
        for (int i =0; i < _chefAgents.Length; i++)
        {
            _chefAgents[i].transform.position = _spawnPoints[i].position;
        }
    }
}
