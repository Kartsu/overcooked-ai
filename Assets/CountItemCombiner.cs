﻿using System.Collections.Generic;
using UnityEngine;

internal class CountItemCombiner : MonoBehaviour, IItemCombiner 
{
    //TODO: Use Zenject for this (dependency inversion)
    //Temporary method to reward the agent.
    [SerializeField]
    ChefAgent _chefAgent = default;

    [SerializeField]
    int _itemsNeeded = 3;

    [SerializeField]
    Sprite[] _sprites = default;

    SpriteRenderer _spriteRenderer = default;

    public bool Combine(Item item, List<Item> items, ChefStateMachine chefStateMachine)
    {
        if (items.Count + 1 == _itemsNeeded)
        {
            chefStateMachine.SetHeldItem(Item.None);
            return true;
        }
        else
        {
            AddItem(items, chefStateMachine);
            return false;
        }
    }

    void AddItem(List<Item> items, ChefStateMachine chefStateMachine)
    {
        _chefAgent.RewardAgent(0.1f);
        items.Add(Item.Tomato);
        chefStateMachine.SetHeldItem(Item.None);
        UpdateVisuals(items);
    }

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void UpdateVisuals(List<Item> items)
    {
        _spriteRenderer.sprite = _sprites[items.Count];
    }
}