﻿using UnityEngine;
using UnityEngine.Events;

public class Dispenser : MonoBehaviour, IInteractable
{
    [SerializeField]
    UnityEvent<Dispenser> _destroyed = new UnityEvent<Dispenser>();
    public event UnityAction<Dispenser> Destroyed
    {
        add { _destroyed.AddListener(value); }
        remove { _destroyed.RemoveListener(value); }
    }
    void OnDestroyed()
    {
        _destroyed.Invoke(this);
        Destroy(this);
    }

    [SerializeField]
    int _numItemsInDispenser = -1;
    [SerializeField]
    Item _itemToDispense = default;

    public void Construct(int numItemsInDispenser, Item itemToDispense)
    {
        _numItemsInDispenser = numItemsInDispenser;
        _itemToDispense = itemToDispense;
    }
    public void Interact(ChefStateMachine chefStateMachine)
    {
        if (_numItemsInDispenser > 0 || _numItemsInDispenser < 0)
        {
            if (_numItemsInDispenser > 0) {
                _numItemsInDispenser--;
            }
            chefStateMachine.SetHeldItem(_itemToDispense);
        }
        if(_numItemsInDispenser == 0)
        {
            //Destroy the dispenser
            OnDestroyed();
        }
    }

    public Item GetItemToDispense()
    {
        return _itemToDispense;
    }
}