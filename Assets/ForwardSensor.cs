using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardSensor : MonoBehaviour, ISensor
{

    [SerializeField]
    float _distance = 2.0f;

    [SerializeField]
    LayerMask _layerMask = default;

    ChefStateMachine _chefStateMachine = default;

    public RaycastHit2D Sense()
    {
        Vector2 dir = new Vector2(0,0);
        if(_chefStateMachine.GetPreviousMoveDirection() == Movement.Left)
        {
            dir = new Vector2(-1, 0);
        }
        else if (_chefStateMachine.GetPreviousMoveDirection() == Movement.Right)
        {
            dir = new Vector2(1, 0);
        }
        else if (_chefStateMachine.GetPreviousMoveDirection() == Movement.Up)
        {
            dir = new Vector2(0, 1);
        }
        else if (_chefStateMachine.GetPreviousMoveDirection() == Movement.Down)
        {
            dir = new Vector2(0, -1);
        }
        Debug.DrawRay(transform.position, dir * _distance, Color.blue);
        return Physics2D.Raycast(transform.position, dir, _distance, _layerMask);
    }

    // Start is called before the first frame update
    void Start()
    {
        _chefStateMachine = GetComponentInParent<ChefStateMachine>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
