﻿internal interface IInputManager
{
    bool GetAcceptInputs();
    void SetAcceptInputs(bool isInputAccepted);
    void SetActions(float[] actionsOut);
}