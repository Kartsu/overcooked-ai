﻿internal interface IInteractable
{
    void Interact(ChefStateMachine chefStateMachine);
}