﻿using System.Collections.Generic;

internal interface IItemCombiner
{
    bool Combine(Item item, List<Item> items, ChefStateMachine chefStateMachine);
}