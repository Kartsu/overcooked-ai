﻿internal interface IMover
{
    void Move(float[] act);
}