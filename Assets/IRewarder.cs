﻿internal interface IRewarder
{
    void AddReward(float reward);

    void SetReward(float reward);
}