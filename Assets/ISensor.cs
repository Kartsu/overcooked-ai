﻿using UnityEngine;

internal interface ISensor
{
    RaycastHit2D Sense();
}