﻿internal interface IVisuals
{
    void UpdateVisuals(float[] act);

    void UpdateVisuals();
}