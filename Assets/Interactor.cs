﻿using UnityEngine;

internal class Interactor : MonoBehaviour
{
    ISensor _sensor;

    public void Interact(ChefStateMachine chefStateMachine)
    {
        RaycastHit2D _hit = _sensor.Sense();
        if(_hit)
        {
            if(_hit.collider.GetComponent<IInteractable>() != null)
            {
                foreach (IInteractable interactable in _hit.collider.GetComponents<IInteractable>())
                {
                    interactable.Interact(chefStateMachine);
                }
            }
        }
    }

    private void Start()
    {
        _sensor = GetComponentInChildren<ISensor>();
    }
}