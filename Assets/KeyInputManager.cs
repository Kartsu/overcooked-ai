﻿using UnityEngine;

internal class KeyInputManager : MonoBehaviour, IInputManager
{
    [SerializeField]
    bool _isInputedAccepted = true;
    public void SetAcceptInputs(bool isInputAccepted)
    {
        _isInputedAccepted = isInputAccepted;
    }

    public bool GetAcceptInputs()
    {
        return _isInputedAccepted;
    }

    public void SetActions(float[] actionsOut)
    {
        actionsOut[0] = (int) Movement.NoAction;
        actionsOut[1] = (int) Action.NoAction;
        if (_isInputedAccepted)
        {
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                actionsOut[0] = (int)Movement.Right;
            }
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                actionsOut[0] = (int)Movement.Up;
            }
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                actionsOut[0] = (int)Movement.Left;
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                actionsOut[0] = (int)Movement.Down;
            }
            if (Input.GetKey(KeyCode.Space))
            {
                actionsOut[1] = (int)Action.Interact;
            }
        }
    }
}