﻿using UnityEngine;

internal class MultiAgentRewarder : MonoBehaviour, IRewarder
{
    ChefAgent _chefAgent;

    [SerializeField]
    ChefAgent _otherChefAgent = default;

    public void AddReward(float reward)
    {
        _chefAgent.AddReward(reward);
        _otherChefAgent.AddReward(reward);
    }

    public void SetReward(float reward)
    {
        _chefAgent.SetReward(reward);
        _otherChefAgent.SetReward(reward);
    }

    void Start()
    {
        _chefAgent = GetComponent<ChefAgent>();
    }
}