﻿using System;
using Unity.MLAgents.Sensors;
using UnityEngine;

internal class ObservationCollector : MonoBehaviour
{
    Rigidbody2D rb;
    ChefStateMachine chefStateMachine;
    //Lazy. This only allows for one pot.
    [SerializeField]
    Pot _pot = default;
    [SerializeField]
    ObservationCollector _otherChef = default;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        chefStateMachine = GetComponent<ChefStateMachine>();
    }
    public void AddObservations(VectorSensor sensor)
    {
        sensor.AddObservation(gameObject.transform.localPosition);
        sensor.AddObservation(rb.velocity);
        sensor.AddOneHotObservation((int)chefStateMachine.GetHeldItem(), Enum.GetNames(typeof(Item)).Length);

        _otherChef.AddObservationsAsOtherChef(sensor);

        sensor.AddObservation(_pot.GetItems().Count);
        sensor.AddOneHotObservation((int)_pot.GetItemHolderState(), Enum.GetNames(typeof(ItemHolderState)).Length);
    }

    public void AddObservationsAsOtherChef(VectorSensor sensor)
    {
        sensor.AddObservation(gameObject.transform.localPosition);
        sensor.AddObservation(rb.velocity);
        sensor.AddOneHotObservation((int)chefStateMachine.GetHeldItem(), Enum.GetNames(typeof(Item)).Length);
    }
}