using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using UnityEngine;

public class Pot : MonoBehaviour, IInteractable, IStateHolder
{

    [SerializeField]
    Item _combinedItem = Item.TomatoSoup;

    [SerializeField]
    Sprite _emptyStateSprite = default;

    [SerializeField]
    Sprite _combinedStateSprite = default;

    List<Item> _items = new List<Item>();

    SpriteRenderer _spriteRenderer = default;

    IItemCombiner _combiner = default;

    ItemHolderState _itemHolderState = default;

    public void EnvironmentReset()
    {
        Reset();
    }

    public void Interact(ChefStateMachine chefStateMachine)
    {
        if (chefStateMachine.GetHeldItem() == Item.Tomato && _itemHolderState == ItemHolderState.NotFull)
        {
            if(_combiner.Combine(chefStateMachine.GetHeldItem(), _items, chefStateMachine))
            {
                _items = new List<Item>();
                _spriteRenderer.sprite = _combinedStateSprite;
                _itemHolderState = ItemHolderState.Full;
            }
        }
        if(chefStateMachine.GetHeldItem() == Item.Plate && _itemHolderState == ItemHolderState.Full)
        {
            chefStateMachine.SetHeldItem(_combinedItem);
            Reset();
        }
    }

    public List<Item> GetItems()
    {
        return _items;
    }
    public ItemHolderState GetItemHolderState()
    {
        return _itemHolderState;
    }

    void Awake()
    {
        _combiner = GetComponent<IItemCombiner>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        Academy.Instance.OnEnvironmentReset += EnvironmentReset;
    }

    void Reset()
    {
        _items = new List<Item>();
        _spriteRenderer.sprite = _emptyStateSprite;
        _itemHolderState = ItemHolderState.NotFull;
    }
}

public enum ItemHolderState
{
    NotFull,
    Full
}
