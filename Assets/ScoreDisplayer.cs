using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.MLAgents;
using System;

public class ScoreDisplayer : MonoBehaviour
{

    //Lazy. This should exist within a game manager
    int _dishesDelivered = 0;
    TextMeshProUGUI _textMeshProUGUI;
    // Start is called before the first frame update
    void Awake()
    {
        _textMeshProUGUI = GetComponent<TextMeshProUGUI>();
        Academy.Instance.OnEnvironmentReset += EnvironmentReset;
    }

    public void EnvironmentReset()
    {
        _dishesDelivered = 0;
        UpdateScore();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateScore()
    {
        _textMeshProUGUI.text = $"Dishes delivered: {_dishesDelivered}";
    }

    public void IncrementThenUpdateScore()
    {
        _dishesDelivered++;
        UpdateScore();
    }
}
