﻿using UnityEngine;
using UnityEngine.Events;

internal class Server : MonoBehaviour, IInteractable
{
    [SerializeField]
    UnityEvent _served = default;
    public event UnityAction served
    {
        add { _served.AddListener(value); }
        remove { _served.RemoveListener(value); }
    }
    void OnServed()
    {
        _served.Invoke();
    }
    public void Interact(ChefStateMachine chefStateMachine)
    {
        //This is pretty lazy. Eventually have to change items from enums to an interface
        //so they can have a "completed/deliverable dish" type.
        if (chefStateMachine.GetHeldItem() == Item.TomatoSoup)
        {
            OnServed();
            chefStateMachine.SetHeldItem(Item.None);
        }
    }
}