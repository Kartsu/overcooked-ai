﻿using UnityEngine;

internal class SingleAgentRewarder : MonoBehaviour, IRewarder
{
    ChefAgent _chefAgent;
    public void AddReward(float reward)
    {
        _chefAgent.AddReward(reward);
    }

    public void SetReward(float reward)
    {
        _chefAgent.SetReward(reward);
    }

    void Start()
    {
        _chefAgent = GetComponent<ChefAgent>();
    }
}