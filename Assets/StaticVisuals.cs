﻿using UnityEngine;

internal class StaticVisuals : MonoBehaviour, IVisuals
{
    [Tooltip("0: Up, 1: Down, 2: Left, 3: Right")]
    [SerializeField]
    Sprite[] _sprites = default;
    SpriteRenderer _spriteRenderer = default;
    ChefStateMachine _stateMachine = default;
    

    public void UpdateVisuals(float[] act)
    {
        int _movement = Mathf.FloorToInt(act[0]);
        // Look up the index in the movement action list:
        if (_movement == (int) Movement.Left)
        {
            MoveLeft();
        }
        if (_movement == (int) Movement.Right)
        {
            MoveRight();
        }
        if (_movement == (int) Movement.Down)
        {
            MoveDown();
        }
        if (_movement == (int) Movement.Up)
        {
            MoveUp();
        }
        if (_movement != (int) Movement.NoAction)
        {
            _stateMachine.SetPreviousMoveDirection((Movement)_movement);
        }
    }

    public void UpdateVisuals()
    {
        Movement _previousMove = _stateMachine.GetPreviousMoveDirection();
        if(_previousMove == Movement.Up)
        {
            MoveUp();
        }
        if (_previousMove == Movement.Right)
        {
            MoveRight();
        }
        if (_previousMove == Movement.Down)
        {
            MoveDown();
        }
        if (_previousMove == Movement.Left)
        {
            MoveLeft();
        }
    }

    void MoveUp()
    {
        if (_stateMachine.GetHeldItem() == Item.None)
        {
            _spriteRenderer.sprite = _sprites[0];
        }
        else if (_stateMachine.GetHeldItem() == Item.Tomato)
        {
            _spriteRenderer.sprite = _sprites[4];
        }
        else if (_stateMachine.GetHeldItem() == Item.Plate)
        {
            _spriteRenderer.sprite = _sprites[8];
        }
        else if (_stateMachine.GetHeldItem() == Item.TomatoSoup)
        {
            _spriteRenderer.sprite = _sprites[12];
        }
    }

    void MoveDown()
    {
        if (_stateMachine.GetHeldItem() == Item.None)
        {
            _spriteRenderer.sprite = _sprites[1];
        }
        else if(_stateMachine.GetHeldItem() == Item.Tomato)
        {
            _spriteRenderer.sprite = _sprites[5];
        }
        else if (_stateMachine.GetHeldItem() == Item.Plate)
        {
            _spriteRenderer.sprite = _sprites[9];
        }
        else if (_stateMachine.GetHeldItem() == Item.TomatoSoup)
        {
            _spriteRenderer.sprite = _sprites[13];
        }
    }

    void MoveLeft()
    {
        if (_stateMachine.GetHeldItem() == Item.None)
        {
            _spriteRenderer.sprite = _sprites[2];
        }
        else if (_stateMachine.GetHeldItem() == Item.Tomato)
        {
            _spriteRenderer.sprite = _sprites[6];
        }
        else if (_stateMachine.GetHeldItem() == Item.Plate)
        {
            _spriteRenderer.sprite = _sprites[10];
        }
        else if (_stateMachine.GetHeldItem() == Item.TomatoSoup)
        {
            _spriteRenderer.sprite = _sprites[14];
        }
    }

    void MoveRight()
    {
        if (_stateMachine.GetHeldItem() == Item.None)
        {
            _spriteRenderer.sprite = _sprites[3];
        }
        else if (_stateMachine.GetHeldItem() == Item.Tomato)
        {
            _spriteRenderer.sprite = _sprites[7];
        }
        else if (_stateMachine.GetHeldItem() == Item.Plate)
        {
            _spriteRenderer.sprite = _sprites[11];
        }
        else if (_stateMachine.GetHeldItem() == Item.TomatoSoup)
        {
            _spriteRenderer.sprite = _sprites[15];
        }
    }

    void Start()
    {
        _stateMachine = GetComponentInParent<ChefStateMachine>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
}