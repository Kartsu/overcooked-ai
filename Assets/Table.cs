using System;
using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using UnityEngine;
using Zenject;

public class Table : MonoBehaviour, IInteractable, IStateHolder
{

    [Inject]
    GameSettings _gameSettings = default;

    TableVisuals _tableVisuals;
    public void EnvironmentReset()
    {
        _tableVisuals.UpdateVisuals();
    }

    public void Interact(ChefStateMachine chefStateMachine)
    {
        if (_gameSettings._enableItemPlacement)
        {
            Item heldItem = chefStateMachine.GetHeldItem();
            if (heldItem != Item.None)
            {
                Dispenser dispenser = gameObject.AddComponent(typeof(Dispenser)) as Dispenser;
                dispenser.Construct(1, heldItem);
                dispenser.Destroyed += _tableVisuals.UpdateVisuals;
                gameObject.tag = heldItem.ToString();
                chefStateMachine.SetHeldItem(Item.None);
                _tableVisuals.UpdateVisuals();
            }
            else
            {
                gameObject.tag = "Table";
            }
        }
    }

    void Awake()
    {
        _tableVisuals = GetComponent<TableVisuals>();
        Academy.Instance.OnEnvironmentReset += EnvironmentReset;
    }
}
