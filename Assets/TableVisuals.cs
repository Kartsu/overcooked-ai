using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableVisuals : MonoBehaviour
{

    SpriteRenderer _spriteRenderer;

    [SerializeField]
    Sprite[] _sprites = default;

    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void UpdateVisuals(Dispenser dispenser)
    {
        List<Dispenser> dispensers = new List<Dispenser>();
        GetComponents(dispensers);
        dispensers.Remove(dispenser);
        UpdateVisuals(dispensers);
    }

    public void UpdateVisuals()
    {
        List<Dispenser> dispensers = new List<Dispenser>();
        GetComponents(dispensers);
        UpdateVisuals(dispensers);
    }

    void UpdateVisuals(List<Dispenser> dispensers)
    {
        if(dispensers.Count == 0)
        {
            _spriteRenderer.sprite = _sprites[0];
        }
        else
        {
            Item dispenserItem = dispensers[0].GetItemToDispense();
            if (dispenserItem == Item.Plate)
            {
                _spriteRenderer.sprite = _sprites[1];
            }
            else if(dispenserItem == Item.Tomato)
            {
                _spriteRenderer.sprite = _sprites[2];
            }
            else if (dispenserItem == Item.TomatoSoup)
            {
                _spriteRenderer.sprite = _sprites[3];
            }
        }
    }


}
